#from sqlalchemy.orm import relationship, backref


from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

#   setting up root class for declarative declaration
Base = declarative_base()

class FILM_GENRE(Base):

    __tablename__ = "film_genre"

    id = Column(Integer, primary_key=True, unique=True)
    genre = Column(String(50), nullable=False, unique=True)

class GENERAL_COUNTRY(Base):

    __tablename__ = "general_country"

    id = Column(Integer, primary_key=True, unique=True)
    country = Column(String(100), nullable=False, unique=True)

class GENERAL_LANGUAGE(Base):

    __tablename__ = "general_language"

    id = Column(Integer, primary_key=True, unique=True)
    language = Column(String(50), nullable=False, unique=True)

class PERSON_GENDER(Base):

    __tablename__ = "person_gender"

    id = Column(Integer, primary_key=True, unique=True)
    gender = Column(String(50), nullable=False, unique=True)

class PERSON_NATIONALITY(Base):

    __tablename__ = "person_nationality"

    id = Column(Integer, primary_key=True, unique=True)
    nationality = Column(String(100), nullable=False, unique=True)

class PERSON_SALUTATION(Base):

    __tablename__ = "person_salutation"

    id = Column(Integer, primary_key=True, unique=True)
    salutation = Column(String(50), nullable=False, unique=True)

class PERSON_TITLE(Base):

    __tablename__ = "person_title"

    id = Column(Integer, primary_key=True, unique=True)
    title = Column(String(50), nullable=False, unique=True)

class PERSON_HAIR_COLOR(Base):

    __tablename__ = "person_hair_color"

    id = Column(Integer, primary_key=True, unique=True)
    hair_color = Column(String(50), nullable=False, unique=True)

class PERSON_EYE_COLOR(Base):

    __tablename__ = "person_eye_color"

    id = Column(Integer, primary_key=True, unique=True)
    eye_color = Column(String(50), nullable=False, unique=True)

class PERSON_RELIGION(Base):

    __tablename__ = "person_religion"

    id = Column(Integer, primary_key=True, unique=True)
    religion = Column(String(50), nullable=False, unique=True)

class PERSON_RELATIONSHIP_STATUS(Base):

    __tablename__ = "person_relationship_status"

    id = Column(Integer, primary_key=True, unique=True)
    relationship_status = Column(String(100), nullable=False, unique=True)

class PERSON_RELATIONSHIP(Base):

    __tablename__ = "person_relationship"

    id = Column(Integer, primary_key=True, unique=True)
    relationship = Column(String(100), nullable=False, unique=True)

class GENERAL_REGION(Base):

    __tablename__ = "general_region"

    id = Column(Integer, primary_key=True, unique=True)
    region = Column(String(100), nullable=False, unique=True)

class GENERAL_AWARD(Base):

    __tablename__ = "general_award"

    id = Column(Integer, primary_key=True, unique=True)
    award = Column(String(100), nullable=False, unique=True)
