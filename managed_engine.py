from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

''' setting up root class for declarative declaration '''
Base = declarative_base()

class ManagedEngine(object):
    def __init__(self,
                 dbms=None,
                 dbdriver=None,
                 dbuser=None,
                 dbuser_pwd=None,
                 db_server_host=None,
                 dbport=None,
                 db_name=None,
                 echo_verbose=True):
        
        self.dbms = dbms
        self.dbdriver = dbdriver
        self.dbuser = dbuser
        self.dbuser_pwd = dbuser_pwd
        self.db_server_host = db_server_host
        self.dbport = dbport
        self.db_name = db_name
        self.echo_verbose = echo_verbose
        url = '{}+{}://{}:{}@{}:{}/{}'.format(
            self.dbms, self.dbdriver, self.dbuser, self.dbuser_pwd, self.db_server_host, self.dbport, self.db_name)

        self._engine = create_engine(url, echo=self.echo_verbose)
        
        # I have to persist all tables and create them
        Base.metadata.create_all(self._engine)
