import sys
 
from PyQt4.QtCore import QTimer, QObject, pyqtSignal 
from managed_data_manipulation import MasterDataManipulation

class Worker(QObject):

    notify_item = pyqtSignal(object, object)
    finish_progress = pyqtSignal()
   
    def __init__(self, category=None,
                 combo_box=None,
                 parent=None):
        QObject.__init__(self, parent)

        self.category=category
        self.combo_box=combo_box
        
    def init_object(self):
        
        id_worker = id(self)
        
        master_data_manipulation = MasterDataManipulation(worker_id=id_worker)
        query_data=master_data_manipulation.select_all
        self._element = query_data(self.category)

        self.timer = QTimer()

        self.timer.setSingleShot(False)
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.increment)
        self.timer.start()
           
    def increment(self):
 
        try:
            self.notify_item.emit(next(self._element), self.combo_box)
 
        except StopIteration:
            self.timer.stop()
            self.finish_progress.emit()
            self.master_data_manipulation = None
       
    def stop(self):

        self.timer.stop()
