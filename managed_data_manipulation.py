from defined_session import *
from managed_table_models import FILM_GENRE, GENERAL_COUNTRY, GENERAL_LANGUAGE, \
                                                PERSON_GENDER, PERSON_NATIONALITY, PERSON_SALUTATION, \
                                                PERSON_TITLE, PERSON_HAIR_COLOR, PERSON_EYE_COLOR, \
                                                PERSON_RELIGION, PERSON_RELATIONSHIP_STATUS, GENERAL_REGION, \
                                                GENERAL_AWARD, PERSON_RELATIONSHIP


class MasterDataManipulation(object):

    def __init__(self, worker_id=None):

        self._worker_id=worker_id

        self._session = Session()

    def select_all(self, category):
        print "ID Session {} - ID Worker {} \n".format(id(self._session),self._worker_id)

        dict_store_session_query = {'person_gender':               lambda: self._session.query(PERSON_GENDER),
                                     'person_nationality':          lambda: self._session.query(PERSON_NATIONALITY),
                                     'person_salutation':           lambda: self._session.query(PERSON_SALUTATION),
                                     'person_title':                lambda: self._session.query(PERSON_TITLE),
                                     'person_hair_color':           lambda: self._session.query(PERSON_HAIR_COLOR),
                                     'person_eye_color':            lambda: self._session.query(PERSON_EYE_COLOR),
                                     'person_religion':             lambda: self._session.query(PERSON_RELIGION),
                                     'person_relationship_status':  lambda: self._session.query(PERSON_RELATIONSHIP_STATUS)}

        try:

            for record in dict_store_session_query[category]():
                if category == 'person_gender':
                    yield record.id, record.gender
                    #self._session.commit()
                if category == 'person_nationality':
                    yield record.id, record.nationality
                    #self._session.commit()
                if category == 'person_salutation':
                    yield record.id, record.salutation
                    #self._session.commit()
                if category == 'person_title':
                    yield record.id, record.title
                    #self._session.commit()
                if category == 'person_hair_color':
                    yield record.id, record.hair_color
                    #self._session.commit()
                if category == 'person_eye_color':
                    yield record.id, record.eye_color
                    #self._session.commit()
                if category == 'person_religion':
                    yield record.id, record.religion
                    #self._session.commit()
                if category == 'person_relationship_status':
                    yield record.id, record.relationship_status
                    #self._session.commit()
                    
            self._session.commit()

        except Exception:
            self.session.rollback()


        return
